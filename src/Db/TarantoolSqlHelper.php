<?php

/**
 * @author Mikhail Kriachek <mikkie.k@gmail.com>
 * @version 1.0.0
 * @created 14.11.2017
 * @updated null
 */

declare(strict_types=1);

/**
 * @todo: add fields support https://tarantool.org/doc/1.7/book/box/box_space.html
 */
namespace Vuebix\Db{

    use Bitrix\Main\Entity;
    use Bitrix\Main\DB;

    class TarantoolSqlHelper extends DB\MysqlCommonSqlHelper{
        /**
         * Returns an identificator escaping left character.
         *
         * @return string
         * @since 1.0.0
         */
        public function getLeftQuote(){
            return '"';
        }

        /**
         * Returns an identificator escaping right character.
         *
         * @return string
         * @since 1.0.0
         */
        public function getRightQuote(){
            return '"';
        }

        /**
         * Escapes special characters in a string for use in an SQL statement.
         *
         * @param string $value Value to be escaped.
         * @param integer $maxLength Limits string length if set.
         * @return string
         * @since 1.0.0
         */
        public function forSql($value, $maxLength = 0): string{
            if($maxLength > 0){
                $value = substr($value, 0, $maxLength);
            }

            $return = '';
            for($i = 0; $i < strlen($value); ++$i){
                $char = $value[$i];
                $ord = ord($char);
                if($char !== "'" && $char !== "\"" && $char !== '\\' && $ord >= 32 && $ord <= 126){
                    $return .= $char;
                }
                else{
                    $return .= '\\x' . dechex($ord);
                }
            }

            return $return;
        }

        /**
         * Returns instance of a descendant from Entity\ScalarField
         * that matches database type.
         *
         * @param string $name Database column name.
         * @param mixed $type Database specific type.
         * @param array $parameters Additional information.
         * @return Entity\ScalarField
         * @since 1.0.0
         */
        public function getFieldByColumnType($name, $type, array $parameters = null){
            switch($type){
                case 'integer':
                    return new Entity\IntegerField($name);
                    break;

                //    return new Entity\FloatField($name);
                //    return new Entity\DatetimeField($name);
                //    return new Entity\DateField($name);
            }

            return new Entity\StringField($name);
        }
    }
}