<?php

/**
 * @author Mikhail Kriachek <mikkie.k@gmail.com>
 * @version 1.0.0
 * @created 14.11.2017
 * @updated null
 */

declare(strict_types=1);

/**
 * CREATE TABLE test5 (ID INTEGER PRIMARY KEY, NAME VARCHAR(32))
 * https://github.com/tarantool/tarantool/blob/1.8/test/sql/iproto.result
 * https://tarantool.org/en/doc/1.8/dev_guide/internals/sql_protocol.html
 *
 * @todo implement binds if it's possible
 * @todo implement transactions (A multi-statement transaction can not use multiple storage engines)
 * @todo implement indexes
 */
namespace Vuebix\Db {

    use \Bitrix\Main;
    use \Bitrix\Main\Entity;
    use \Bitrix\Main\DB\Result;
    use \Bitrix\Main\DB\Connection;
    use \Bitrix\Main\DB\SqlQueryException;
    use \Bitrix\Main\DB\ConnectionException;

    class TarantoolConnection extends Connection{

        /**
         * Establishes a connection to the database.
         * Includes php_interface/after_connect_d7.php on success.
         * Throws exception on failure.
         *
         * @throws \Bitrix\Main\DB\ConnectionException
         * @return void
         * @since 1.0.0
         */
        protected function connectInternal(){
            if ($this->isConnected){
                return;
            }

            if(!extension_loaded('tarantool') || !class_exists('\\Tarantool')){
                throw new ConnectionException('Tarantool library not loaded. @see https://github.com/tarantool/tarantool-php');
            }

            try{
                $connection = new \Tarantool(
                    $this->host,
                    $this->configuration['port'] ?? 3301,
                    $this->login,
                    $this->password,
                    $this->configuration['sid'] ?? 'tarantool'
                );

                $this->isConnected = true;
                $this->resource = $connection;

                $this->afterConnected();
            }
            catch(\Exception $e){
                throw new ConnectionException(sprintf('Tarantool connect error [%s]', $this->host), $e->getMessage());
            }
        }

        /**
         * Disconnects from the database.
         * Does nothing if there was no connection established.
         *
         * @return void
         * @since 1.0.0
         */
        protected function disconnectInternal(): void{
            if (!$this->isConnected){
                return;
            }

            $this->isConnected = false;
            $this->resource->disconnect();
        }

        /**
         * Executes a query against connected database.
         * Rises SqlQueryException on any database error.
         * When object $trackerQuery passed then calls its startQuery and finishQuery
         * methods before and after query execution.
         *
         * @param string                            $sql Sql query.
         * @param array                             $binds Array of binds.
         * @param \Bitrix\Main\Diag\SqlTrackerQuery $trackerQuery Debug collector object.
         * @throws \Bitrix\Main\Db\SqlQueryException
         * @return array
         * @since 1.0.0
         *
         * @todo: add tracker
         */
        protected function queryInternal($sql, array $binds = null, \Bitrix\Main\Diag\SqlTrackerQuery $trackerQuery = null): array{
            try{
                $data = $this->queryInternalData($sql);
                if(array_key_exists('metadata', $data)){
                    $result = [];
                    $metadata = array_column($data['metadata'], 'name');
                    foreach($data['rows'] as $row){
                        $result[] = array_combine($metadata, $row);
                    }
                }
                else{
                    $result = $data;
                }
            }
            catch(\Exception $e){
                throw new SqlQueryException("", $e->getMessage(), $sql);
            }

            $this->lastQueryResult = $result;

            return $result;
        }

        /**
         * Internal SQL query to tarantool without result handling.
         *
         * @param $sql
         * @return array
         * @since 1.0.0
         */
        protected function queryInternalData($sql): array{
            /**
             * @note lua doesn't support multilines inside of function. We should use sql query in 1 string
             */
            $sql = trim(preg_replace('/\s+/', ' ', $sql));

            return $this->evaluate(sprintf("
                remote = require('net.box')
                connection = remote.connect(box.cfg.listen, {user = '%s', password = '%s'})
                result = connection:execute('%s')
                connection:close()
                return result
            ", addslashes($this->login), addslashes($this->password), addslashes($sql)))[0];
        }

        /**
         * Call low level API of tarantool.
         * lua can be used.
         *
         * @param $query
         * @throws SqlQueryException
         * @return mixed
         * @since 1.0.0
         */
        public function evaluate($query){
            $this->connectInternal();

            try{
                $result = $this->resource->evaluate($query);
            }
            catch(\Exception $e){
                throw new SqlQueryException("", $e->getMessage(), $query);
            }

            return $result;
        }

        /**
         * Returns database depended result of the query.
         *
         * @param resource $result Result of internal query function.
         * @param \Bitrix\Main\Diag\SqlTrackerQuery $trackerQuery Debug collector object.
         * @return TarantoolResult
         * @since 1.0.0
         */
        protected function createResult($result, \Bitrix\Main\Diag\SqlTrackerQuery $trackerQuery = null): TarantoolResult{
            return new TarantoolResult($result, $this, $trackerQuery);
        }

        /**
         * Executes a query to the database.
         * - query($sql)
         * - query($sql, $limit)
         * - query($sql, $offset, $limit)
         * - query($sql, $binds)
         * - query($sql, $binds, $limit)
         * - query($sql, $binds, $offset, $limit)
         *
         * @param string $sql Sql query.
         * @param array $binds Array of binds.
         * @param int $offset Offset of the first row to return, starting from 0.
         * @param int $limit Limit rows count.
         * @throws \Bitrix\Main\Db\SqlQueryException
         * @return Result
         * @since 1.0.0
         */
        public function query($sql): Result{
            [$sql, $binds, $offset, $limit] = self::parseQueryFunctionArgs(func_get_args());

            return parent::query($sql, null, $offset, $limit);
        }

        /**
         * Adds row to table and returns ID of the added row.
         * $identity parameter must be null when table does not have autoincrement column.
         *
         * @param string $tableName Name of the table for insertion of new row.
         * @param array $data Array of columnName => Value pairs.
         * @param string $identity For Oracle only.
         * @throws \Bitrix\Main\Db\SqlQueryException
         * @return int
         * @since 1.0.0
         */
        public function add($tableName, array $data, $identity = "ID"){
            $tableName = trim(preg_replace("/[^a-z0-9_]+/i", "", $tableName));

            if($identity !== null && !isset($data[$identity])){
                $data[$identity] = $this->getNextId($tableName, $identity);
            }

            $insert = $this->getSqlHelper()->prepareInsert($tableName, $data);

            $sql = sprintf("INSERT INTO %s (%s) VALUES(%s)", $tableName, $insert[0], $insert[1]);
            $this->queryExecute($sql);

            $this->lastInsertedId = $data[$identity];

            return $data[$identity];
        }

        /**
         * Gets next value from the database sequence.
         * Sequence name may contain only A-Z,a-z,0-9 and _ characters.
         *
         * @param $tableName
         * @param $identity
         * @throws \Bitrix\Main\ArgumentNullException
         * @return int
         * @since 1.0.0
         */
        public function getNextId($tableName, $identity): int{
            $tableName = trim(preg_replace("/[^a-z0-9_]+/i", "", $tableName));
            $identity = trim(preg_replace("/[^a-z0-9_]+/i", "", $identity));

            if($identity == ''){
                throw new Main\ArgumentNullException("identity");
            }

            if($tableName == ''){
                throw new Main\ArgumentNullException("tableName");
            }

            $sql = sprintf("select max(%s) from %s", $identity, $tableName);

            $result = $this->query($sql);
            if($row = $result->fetch()){
                return array_shift($row) + 1;
            }

            return 1;
        }

        /**
         * Checks if a table exists.
         *
         * @param string $tableName The table name.
         * @return boolean
         * @since 1.0.0
         */
        public function isTableExists($tableName): bool{
            $tableName = trim(preg_replace("/[^a-z0-9_]+/i", "", $tableName));

            if(strlen($tableName) <= 0){
                return false;
            }

            return (bool)$this->query(sprintf("select id from _space where name='%s'", $tableName))->fetch();
        }

        /**
         * Returns database type "tarantool".
         *
         * @return string
         * @see \Bitrix\Main\DB\Connection::getType
         * @since 1.0.0
         */
        public function getType(){
            return "tarantool";
        }

        /**
         * Return ID of last inserted tuple/record.
         *
         * @return int
         * @since 1.0.0
         */
        public function getInsertedId(): int{
            return (int)$this->lastInsertedId;
        }

        /**
         * Returns fields objects according to the columns of a table.
         * Table must exists.
         *
         * @param string $tableName
         * @return array
         * @since 1.0.0
         */
        public function getTableFields($tableName): array{
            $tableName = trim(preg_replace("/[^a-z0-9_]+/i", "", $tableName));
            if(!isset($this->tableColumnsCache[$tableName])){
                $data = $this->evaluate(sprintf("return box.space.%s:format()", $tableName))[0] ?? [];
                /**
                 * @note Workaround coz' we can't use ::getFields(), there's no resource or API to do it
                 */
                $helper = $this->getSqlHelper();
                foreach($data as $field){
                    $this->tableColumnsCache[$tableName][$field['name']] = $helper->getFieldByColumnType($field['name'], $field['type']);
                }
            }

            return $this->tableColumnsCache[$tableName] ?? [];
        }

        /**
         * Create SQL helper.
         *
         * @return TarantoolSqlHelper
         * @since 1.0.0
         */
        protected function createSqlHelper(): TarantoolSqlHelper{
            return new TarantoolSqlHelper($this);
        }

        /**
         * Returns connected database version.
         * Version presented in array of two elements.
         * - First (with index 0) is database version.
         * - Second (with index 1) is true when light/express version of database is used.
         *
         * @throws \Bitrix\Main\Db\SqlQueryException
         * @return array
         * @since 1.0.0
         */
        public function getVersion(): array{
            $result = $this->evaluate("return box.space._schema:select('version')")[0][0];
            array_shift($result);

            return [(string)join('.', $result), null];
        }

        /**
         * Returns affected rows count from last executed query.
         *
         * @return int
         * @since 1.0.0
         */
        public function getAffectedRowsCount(): int{
            return (int)$this->lastQueryResult['rowcount'];
        }

        /**
         * Drops the table.
         *
         * @param string $tableName Name of the table to be dropped.
         * @throws \Bitrix\Main\Db\SqlQueryException
         * @return bool
         * @since 1.0.0
         */
        public function dropTable($tableName): bool{
            $tableName = trim(preg_replace("/[^a-z0-9_]+/i", "", $tableName));
            $this->query('drop table ' . $this->getSqlHelper()->quote($tableName));

            return $this->getAffectedRowsCount() > 0;
        }

        /**
         * Renames the table. Renamed table must exists and new name must not be occupied by any database object.
         *
         * @param string $currentName Old name of the table.
         * @param string $newName New name of the table.
         * @throws \Bitrix\Main\Db\SqlQueryException
         * @return bool
         * @since 1.0.0
         */
        public function renameTable($currentName, $newName): bool{
            $currentName = trim(preg_replace("/[^a-z0-9_]+/i", "", $currentName));
            $newName = trim(preg_replace("/[^a-z0-9_]+/i", "", $newName));

            $this->evaluate(sprintf("return box.space.%s:rename('%s')", $currentName, $newName));
            $this->lastQueryResult = ['rowcount' => 1];

            return true;
        }

        /**
         * @DUMMY!!!
         * @todo implement if possible
         */

        /**
         * @param string $tableName Name of the new table.
         * @param \Bitrix\Main\Entity\ScalarField[] $fields Array with columns descriptions.
         * @param string[] $primary Array with primary key column names.
         * @param string[] $autoincrement Which columns will be auto incremented ones.
         * @throws \Bitrix\Main\ArgumentException
         * @throws \Bitrix\Main\Db\SqlQueryException
         * @return void
         */
        public function createTable($tableName, $fields, $primary = [], $autoincrement = []){
            throw new \Exception('Not Implemented [17112017.2329.1]');

            $tableName = trim(preg_replace("/[^a-z0-9_]+/i", "", $tableName));
            $sql = 'create table ' . $this->getSqlHelper()->quote($tableName).  ' (';
            $sqlFields = [];

            foreach($fields as $columnName => $field){
                if(!($field instanceof Entity\ScalarField)){
                    throw new Main\ArgumentException(sprintf(
                        'Field `%s` should be an Entity\ScalarField instance', $columnName
                    ));
                }

                $realColumnName = $field->getColumnName();

                $sqlFields[] = $this->getSqlHelper()->quote($realColumnName)
                    . ' ' . $this->getSqlHelper()->getColumnTypeByField($field)
                    . ' NOT NULL' // null for oracle if is not primary
                    . (in_array($columnName, $autoincrement, true) ? ' AUTO_INCREMENT' : '')
                ;
            }

            $sql .= join(', ', $sqlFields);

            if(!empty($primary)){
                foreach($primary as &$primaryColumn){
                    $realColumnName = $fields[$primaryColumn]->getColumnName();
                    $primaryColumn = $this->getSqlHelper()->quote($realColumnName);
                }

                $sql .= ', PRIMARY KEY('.join(', ', $primary).')';
            }

            $sql .= ')';

            $this->query($sql);
        }

        public function getIndexName($tableName, array $columns, $strict = false){
            $tableName = trim(preg_replace("/[^a-z0-9_]+/i", "", $tableName));

            throw new \Exception('Not Implemented [14112017.0018.1]');
        }

        public function startTransaction(){
            //return $this->queryInternalData('begin transaction');

            throw new \Exception('Not Implemented [14112017.0014.1]');
        }

        public function commitTransaction(){
            //return $this->queryInternalData('commit');

            throw new \Exception('Not Implemented [14112017.0015.1]');
        }

        public function rollbackTransaction(){
            //return $this->queryInternalData('rollback');

            throw new \Exception('Not Implemented [14112017.0016.1]');
        }

        public function isIndexExists($tableName, array $columns){
            $tableName = trim(preg_replace("/[^a-z0-9_]+/i", "", $tableName));

            throw new \Exception('Not Implemented [14112017.0540.1]');
        }

        protected function getErrorMessage(){
            throw new \Exception('Not Implemented [14112017.0017.1]');
        }
    }
}