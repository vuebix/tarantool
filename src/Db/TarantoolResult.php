<?php

/**
 * @author Mikhail Kriachek <mikkie.k@gmail.com>
 * @version 1.0.0
 * @created 14.11.2017
 * @updated null
 */

declare(strict_types=1);

namespace Vuebix\Db{

    use Bitrix\Main\DB\Result;

    class TarantoolResult extends Result{

        /**
         * @var null
         * @since 1.0.0
         */
        protected $resultFields = null;

        /**
         * TarantoolResult constructor.
         *
         * @param resource $result Database-specific query result.
         * @param TarantoolConnection|null $dbConnection
         * @since 1.0.0
         */
        public function __construct($result, TarantoolConnection $dbConnection = null){
            parent::__construct($result, $dbConnection);
        }

        /**
         * Returns the number of rows in the result.
         *
         * @return int
         * @since 1.0.0
         */
        public function getSelectedRowsCount(): int{
            return count($this->resource);
        }

        /**
         * Returns null because there is no way to know the fields.
         *
         * @return null
         * @since 1.0.0
         */
        public function getFields(){
            return null;
        }

        /**
         * Returns next result row or false.
         *
         * @return array|false
         * @since 1.0.0
         */
        protected function fetchRowInternal(){
            $val = current($this->resource);
            next($this->resource);

            return $val;
        }
    }
}