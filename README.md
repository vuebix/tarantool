# PHP Tarantool adapter for Bitrix

This version of client requires [Tarantool](https://github.com/tarantool/tarantool/) 1.8.2 or above and [PECL PHP driver for Tarantool](https://github.com/tarantool/tarantool-php/)

> *Note*
>
> This library is an example of integration Tarantool with Bitrix.  
> Strongly **NOT RECOMMEND** to use on production environment

## Installation

The recommended way to install the library is through [Composer](http://getcomposer.org):

```sh
$ composer require vuebix/tarantool:@dev
```

## Bitrix settings

Add new connection to /bitrix/.settings.php

```php
'tarantool' =>
    array (
        'className' => '\\Vuebix\\DB\\TarantoolConnection',
        'port': 3301,
        'host' => '<tarantool>',
        'login' => '<login>',
        'password' => '<password>',
        'sid' => 'tarantool',
    ),
```

## Bitrix ORM entity

```php
<?php
 
namespace Test{
 
    use Bitrix\Main\Entity;
 
    class DemoTable extends Entity\DataManager{
 
        public static function getConnectionName(){
            return 'tarantool';
        }
 
        public static function getTableName(){
            return 'table';
        }
 
        public static function getMap(){
            return [
                new Entity\IntegerField('ID', [
                    'primary' => true,
                    'autocomplete' => true
                ]),
                new Entity\StringField('NAME'),
            ];
        }
    }
}
```

## Usage

```php
<?php
 
// Get All records
$result = \Test\DemoTable::getList()->fetchAll();
var_dump($result);
 
// Get record
$result = \Test\DemoTable::getById($id)->fetch();
var_dump($result);
 
// Add new record
$result = \Test\DemoTable::add($data);
var_dump($result->isSuccess());
 
// Update record
\Test\DemoTable::update($id, ['NAME' => $name]);
var_dump($result->isSuccess());
 
// Delete record
$result = \Test\DemoTable::delete($id);
var_dump($result->isSuccess());
```


## License

The library is released under the MIT License. See the bundled [LICENSE](LICENSE) file for details.